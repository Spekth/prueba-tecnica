import mongoose,{Schema} from "mongoose";

const UserDocumentSchema = new Schema({
  UserID : { type: Schema.Types.ObjectId, ref: 'AppUser'},
  Document: { type: String, maxLength: 20},
  TypeDocument: { type: Schema.Types.ObjectId, ref: 'TypeDocument'},
  PlaceExpedition: { type: String, maxLength: 60},
  DateExpedition: { type: Date, default: Date.now()}
},{versionKey:false})

const UserDocument = mongoose.model('UserDocument',UserDocumentSchema);
export default UserDocument;