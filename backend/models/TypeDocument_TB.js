import mongoose,{Schema} from "mongoose";

const TypeDocumentSchema = new Schema({
  NameTypeDocument: { type: String,maxLength:50}
},{versionKey:false})

const TypeDocument = mongoose.model('TypeDocument',TypeDocumentSchema);
export default TypeDocument;