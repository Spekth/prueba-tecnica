import mongoose,{Schema} from "mongoose";

const ContactInfoSchema = new Schema({
  UserID: { type: Schema.Types.ObjectId, ref: 'AppUser'},
  Address: { type: String, maxLength: 60},
  CountryID : { type: Schema.Types.ObjectId, ref: 'Country'},
  City: { type: String, maxLength: 50},
  Phone: { type: String, maxLength: 20},
  CelPhone: { type: String, maxLength: 20},
  EmergencyName: { type: String, maxLength: 100},
  EmergencyPhone: { type: String, maxLength: 20}


},{versionKey:false})

const ContactInfo = mongoose.model('ContactInfo',ContactInfoSchema);
export default ContactInfo;