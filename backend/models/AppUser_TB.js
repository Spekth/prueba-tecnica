import mongoose,{Schema} from "mongoose";

const AppUserSchema = new Schema({
  LastName: { type: String, maxLength:20,required:true},
  Name: { type: String, maxLength: 20},
  IsMilitar: { type: Boolean,default:1},
  IsTemporal: { type: Boolean, default:1},
  username : {type:String},
  password : { type: String,required:true},
  email : { type: String,unique:true,required:true},
  emailVerified: { type: String,required:true},
  verificationToken: { type: String}
},{versionKey:false})

const AppUser = mongoose.model('AppUser',AppUserSchema);
export default AppUser;