import mongoose,{Schema} from "mongoose";

const CountrySchema = new Schema({
  CountryCode: { type: String, maxLength:4},
  CountryName: { type: String, maxLength: 100}
},{versionKey:false})

const Country = mongoose.model('Country',CountrySchema);
export default Country;