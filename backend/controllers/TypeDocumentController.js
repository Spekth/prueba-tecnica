import TypeDocument from '../models/TypeDocument_TB'

export const addTypeDocument = async(req,res,next)=>{
  try {
    const body = req.body;
    const typeDocument = await TypeDocument.create(body);
    res.status(200).json(typeDocument);  
  } catch (err) {
    res.status(500).send({
      message: `An error ocurred ${err}`
   }) 
   next(err);
  }
}

export const getTypeDocuments = async(req,res,next)=>{
  try {
      const typeDocuments = await TypeDocument.find({})
      res.status(200).json(typeDocuments);  
    } catch (err) {
      res.status(500).send({
        message: `An error ocurred ${err}`
      }) 
      next(err);
  }
}
export const updateTypeDocument=  async(req,res,next)=>{
  try {
      const { id } = req.params;
      const update  = req.body;
      const typeDocument  = await TypeDocument.findByIdAndUpdate(id,update,{new:true});
      res.status(200).json(typeDocument);
    } catch (err) {
      res.status(500).send({
          message: `An error ocurred ${err}`
      }) 
      next(err);
  }
}
export const deleteTypeDocument = async(req,res,next)=>{
  try {
      const { id } = req.params;
      await TypeDocument.findByIdAndUpdate(id,{state:false},{new:true});
      res.status(200).json({message:'TypeDocument delete succesfully'});
    } catch (err) {
      res.status(500).send({
          message: `An error ocurred ${err}`
      }) 
      next(err);
  } 
}  
