import Country from '../models/Country_TB'

export const addCountry = async(req,res,next)=>{
  try {
    const body = req.body;
    const country = await Country.create(body);
    res.status(200).json(country);  
  } catch (err) {
    res.status(500).send({
      message: `An error ocurred ${err}`
   }) 
   next(err);
  }
}

export const getCountries = async(req,res,next)=>{
  try {
      const countries = await Country.find({})
      res.status(200).json(countries);  
    } catch (err) {
      res.status(500).send({
        message: `An error ocurred ${err}`
      }) 
      next(err);
  }
}
export const updateCountry=  async(req,res,next)=>{
  try {
      const { id } = req.params;
      const update  = req.body;
      const country  = await Country.findByIdAndUpdate(id,update,{new:true});
      res.status(200).json(country);
    } catch (err) {
      res.status(500).send({
          message: `An error ocurred ${err}`
      }) 
      next(err);
  }
}
export const deleteCountry = async(req,res,next)=>{
  try {
      const { id } = req.params;
      await Country.findByIdAndDelete(id);
      res.status(200).json({message:'Country delete succesfully'});
    } catch (err) {
      res.status(500).send({
          message: `An error ocurred ${err}`
      }) 
      next(err);
  } 
}  
