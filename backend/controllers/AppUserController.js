import AppUser from '../models/AppUser_TB'
import bcrypt from 'bcryptjs'

export const addAppUser = async(req,res,next)=>{
  try {
    let body = req.body;
    /* let salt = bcrypt.genSaltSync(10); */
    /* body.password = await bcrypt.hashSync(body.password,salt); */
    body.password =  bcrypt.hashSync(body.password, parseInt(salt));
    const salt = await bcrypt.genSaltSync(10);
    await req.body.password;
    if(body.email === body.emailVerified ){
      const appUser = await AppUser.create(body);
      res.status(200).json(appUser); 
    }else{
      res.status(404).json({msg:'LOS EMAILS NO COINCIDEN...'}); 
    }
  } catch (err) {
    res.status(500).json(err);
    next(err);
  }
}

export const getAppUsers = async(req,res,next)=>{
  try {
      const appUsers = await AppUser.find({})
      res.status(200).json(appUsers);  
    } catch (err) {
      res.status(500).send({
        message: `An error ocurred ${err}`
      }) 
      next(err);
  }
}
export const updateAppUser=  async(req,res,next)=>{
  try {
      const { id } = req.params;
      const update  = req.body;
      const appUser  = await AppUser.findByIdAndUpdate(id,update,{new:true});
      res.status(200).json(appUser);
    } catch (err) {
      res.status(500).send({
          message: `An error ocurred ${err}`
      }) 
      next(err);
  }
}
export const deleteAppUser = async(req,res,next)=>{
  try {
      const { id } = req.params;
      await AppUser.findByIdAndDelete(id);
      res.status(200).json({message:'AppUser delete succesfully'});
    } catch (err) {
      res.status(500).send({
          message: `An error ocurred ${err}`
      }) 
      next(err);
  } 
}  
