import express from 'express'
import typeDocumentRouter from './typeDocument'
import countryRouter from './country'
import appUserRouter from './appUser'

const router = express.Router()

router.use(typeDocumentRouter)
router.use(countryRouter)
router.use(appUserRouter)


export default router