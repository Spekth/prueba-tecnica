import express,{ Router } from 'express'
import { addAppUser, getAppUsers } from '../controllers/AppUserController'

const router = Router();

router.post('/app-user',addAppUser)
router.get('/app-users',getAppUsers);

export default router;