import express,{ Router } from 'express'
import { addTypeDocument, getTypeDocuments} from '../controllers/TypeDocumentController'

const router = Router();

router.post('/type-document',addTypeDocument)
router.get('/type-documents',getTypeDocuments);

export default router;