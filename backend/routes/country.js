import express,{ Router } from 'express'
import { addCountry, deleteCountry, getCountries } from '../controllers/CountryController'

const router = Router();

router.post('/country',addCountry)
router.get('/countries',getCountries);
router.delete('/country/:id',deleteCountry);

export default router;